#!/usr/bin/perl
use strict;
use warnings;
use JSON;
use Data::Dumper;
use Time::Local;
use Time::HiRes qw(time);
use POSIX qw(strftime);
use Sys::Hostname;
use File::Path qw(make_path remove_tree);
use DateTime;
use Encode qw(decode encode);

use Const;

=pod

    Скрипт выделяет из упрощенных данных eye-tracker саккады и фиксации глаза

=cut

=pod

    - Состояния саккады и фиксации взаимоисключающие.
    - 

    http://dic.academic.ru/dic.nsf/enc_psychology/200/%D0%94%D0%B2%D0%B8%D0%B6%D0%B5%D0%BD%D0%B8%D1%8F

    Маловероятно, что последовательные саккады произойдут в промежуток времени меньше 150 мс, 
    поскольку зрительной системе требуется около 50 мс на программирование саккады в процессе фиксации, 
    от 20 до 30 мс — на выполнение саккадического движения и 50 мс — на восстановление четкого восприятия.


    http://www.vdvs.ru/info/tests/saccade.htm

    Cаккады – очень быстрые движения глаз с длительностью от 10 мсек до 80 мсек.
=cut

=pod

    Алгоритм:
    - Выделяем окно. Все содержимое окна забиваем в память. В дальнейшем, когда часть окна будет отработана, окно будет сдвинуто дальше по файлу
    - Уже имеем с предыдущего этапа предварительно выделенные группы фреймов, где координаты мало изменялись (плато)
    - В рамках окна расширяем группы фреймов плато. Для этого ищем фреймы, которые находятся между плато, находящимися на близких и тех же коордитатах. Помечаем их тоже как плато.

=cut


my $TIME_DIVISOR        = 1000000;                      #на сколько нужно делить печать времени, чтобы получить секунды
my $FRAME_TIME          = 4000;                         #сколько времени (в единицах, представленных в файле) длится 1 кадр

# формат:   t       dt      Х       У       dx      dy      dv      тип_кадра
my @FIELD_NUM           = (0, 1, 2, 3, 4, 5, 6, 7);

my $IN_FNAME            = $Const::OUT_DIR."/out1.txt";
my $OUT_FNAME           = $Const::OUT_DIR."/out2.txt";
my $DEBUG_FNAME         = $Const::DEBUG_DIR."/debug_join_fix.txt";

open (my $F_IN,  "<", $IN_FNAME);
open (my $F_OUT, ">", $OUT_FNAME);
open (my $F_DEBUG, ">", $DEBUG_FNAME);

my $start               = 0;
my $stop                = $start + $Const::WINDOW_SIZE;
my @WINDOW;
my $finish              = 0;
my $load_size           = $stop - $start;
my $types_count;
while ($finish == 0){

    #догружаем в окно данных
    #print "shift window (size = $load_size)\n";
    my $res             = Const::shift_window($F_IN, $F_OUT, \ @WINDOW, $load_size, $types_count);
    if ($res->{status} == 0){
        @WINDOW         = @{$res->{window}};
        $types_count    = $res->{types_count};
    } else {
        print "Error load data (size = $load_size)! status=".$res->{status}." \n";
        exit;
    }
    $finish             = $res->{finish} || 0;
    $stop               = scalar(@WINDOW);

    #анализируем данные.
    for (my $j = 0; $j < 2; $j++){
        #действуем в несколько проходов
        my $i           = $start;
        my @platoes;
        my ($plato_begin, $plato_end) = (0, 0);
        my $plato_count = 0;
        while ($i < $stop){
            if ($WINDOW[$i]->{ty} == 1){
                #предварительно кадр фиксации
                if ($plato_begin == 0){
                    #если начало фиксации не определено - это оно
                    $plato_begin = $i;
                    $plato_end = $i;
                } else {
                    #если начало фиксации определено - переопределяем конец фиксации
                    $plato_end = $i;
                }
            }
            if ($WINDOW[$i]->{ty} != 1){
                #предварительно не кадр фиксации
                if ($plato_begin > 0 && $plato_end > 0){
                    #имеем определенный диапазон кадров фиксации, пишем в массив
                    push @platoes, { b => $plato_begin, e => $plato_end };
                    $plato_count++;
                }
                #обнуляем границы фиксации, так как ищем следующую
                ($plato_begin, $plato_end) = (0, 0);
            }
            $i++;
        }
        #print $F_DEBUG Dumper(@platoes);

        #перебираем выявленные участки плато. Смотрим, можно ли их объединить
        my $av;
        for (my $i = 0; $i < ($plato_count - 1); $i++){
            my ($p1_b, $p1_e) = ($platoes[$i]->{b},     $platoes[$i]->{e});
            my ($p2_b, $p2_e) = ($platoes[$i+1]->{b},   $platoes[$i+1]->{e});
            my ($np_b, $np_e) = ($platoes[$i]->{e} + 1, $platoes[$i+1]->{b} - 1);
            my ($p1_size, $np_size, $p2_size) = ($p1_e - $p1_b, $np_e - $np_b, $p2_e - $p2_b);

            #если для первого плато среднее уже есть, не пересчитываем его
            my $p1_av;
            if (defined $av){
                $p1_av      = $av;
            } else {
                $p1_av      = Const::average(\ @WINDOW, $p1_b, $p1_size, 0);
            }
            #считаем среднее для второго плато и участка между плато
            my $p2_av       = Const::average(\ @WINDOW, $p2_b, $p2_size, 0);
            my $np_av       = Const::average(\ @WINDOW, $np_b, $np_size, 0);

            #запоминаем среднее второго плато чтобы его не пересчитывать, когда будем смотреть следующую пару
            $av             = $p2_av;

            #смотрим, можно ли пометить участок между плато как саккаду
            if (defined $np_av){
                if (($p1_av->{adt} > $Const::MIN_FIXATION_TIME ||               #
                     $p2_av->{adt} > $Const::MIN_FIXATION_TIME) &&              #
                     $np_av->{adt} > $Const::MIN_SACCADE_TIME &&                # если кадров между плато было не меньше, чем должна длиться минимальная саккада
                     $np_av->{adt} < $Const::MAX_SACCADE_TIME &&                # и не больше, чем должна длиться максимальная
                     $np_av->{adv} > $Const::MAX_FIXATION_DV                    # и общее перемещение больше чем допустимое в рамках фиксации
                   ){
                    my $r = Const::mark_frames(\ @WINDOW, $np_b, $np_size, 2);
                    if ($r->{status} == 0){
                        @WINDOW = @{$r->{window}};
                    } else {
                        print "Error mark frames (pos = $np_b, size = $np_size, type = 2)! status=".$r->{status}." \n";
                        exit;
                    }
                }
                elsif ( $np_av->{adv} < $Const::MAX_FIXATION_DV ||              # общее перемещение меньше чем допустимое в рамках фиксации
                        ( $p1_size > $np_size &&                                # длина участка первого фиксации больше чем длина неразмеченной области
                          $p2_size > $np_size &&                                # длина участка второго фиксации больше чем длина неразмеченной области
                          $np_av->{adv} < $Const::MIN_SACCADE_TIME )            # длительность неразмеченного участка слишком короткая для саккады
                      ){
                    my $r = Const::mark_frames(\ @WINDOW, $np_b, $np_size, 1);
                    if ($r->{status} == 0){
                        @WINDOW = @{$r->{window}};
                    } else {
                        print "Error mark frames (pos = $np_b, size = $np_size, type = 1)! status=".$r->{status}." \n";
                        exit;
                    }
                }
            }

=pod
            print $F_DEBUG "($p1_b, $p1_e) - ($np_b, $np_e) - ($p2_b, $p2_e)\n";
            print $F_DEBUG Dumper($p1_av);
            print $F_DEBUG Dumper($np_av);
            print $F_DEBUG Dumper($p2_av);
            print $F_DEBUG "\n\n";
=cut
        }       #for (my $i = 0; $i < ($plato_count - 1); $i++)
    }           #for (my $j = 0; $j < 3; $j++)

    #определяем сколько надо прогрузить в следующей итерации
    $load_size           = 500;
    #$start               = $start + $load_size;
    #$stop                = $res->{loaded} + $stop;

}

#выливаем в выход остатки файла
my $res                 = Const::shift_window($F_IN, $F_OUT, \ @WINDOW, $Const::WINDOW_SIZE, $types_count);
$types_count            = $res->{types_count};

print "Fixation_frames: ".$types_count->{1}."\n";
print "Saccade_frames: ".$types_count->{2}."\n";
print "Not_defined_frames: ".$types_count->{0}."\n";
print "All_frames: ".($types_count->{0} + $types_count->{1} + $types_count->{2})."\n";

close($F_IN);
close($F_OUT);
close($F_DEBUG);

exit;


1;

=pod

    AUTHOR - Мартынов Сергей

=cut

