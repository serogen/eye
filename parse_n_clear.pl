#!/usr/bin/perl
use strict;
use warnings;
use JSON;
use Data::Dumper;
use Time::Local;
use Time::HiRes qw(time);
use POSIX qw(strftime);
use Sys::Hostname;
use File::Path qw(make_path remove_tree);
use DateTime;
use Encode qw(decode encode);
use Const;

=pod

    Скрипт разбирает сырые данные от eye-tracker, выделяет необходимые для дальнейшего анализа поля и сохраняет выделенные данные в другом файле
    При это производится первичное выделение фиксаций
    Также производится чистка невалидных данных. Не валидными данные признаются если:
        - Хотя бы в одном координатном или временном поле присутствуют символы, недопустимые при записи float
        - Хотя бы одно координатное поле равно 0 или не определено
        - Разница соседних значений хотя бы для одного координатного поля больше допустимой
        - Разница скоростей по Х или У для правого и левого глаза больше несущественной

=cut

my @FIELD_NUM           = (0, 21, 22, 23, 24);                          #номера полей, которые нужно выдергивать
my @FIELD_NUM_OUT       = (0, 21, 22);                                  #номера полей, которые нужно писать на выход (из тех, что были на входе)

my $MAX_DELTA_PERCENT   = {                                             #максимальное допустимое изменение полей в %
                            0   => 0,                                   #время
                            21  => 10,                                  #Х левого глаза
                            22  => 10,                                  #У левого глаза
                            23  => 10,                                  #Х правого глаза
                            24  => 10,                                  #У правого глаза
                          };
my $MAX_DELTA           = {                                             #максимальное допустимое изменение полей за 1 кадр
                            0   => 0,                                   #время
                            21  => $Const::WIDTH * $MAX_DELTA_PERCENT->{21} / 100,   #Х левого глаза
                            22  => $Const::HEIGHT * $MAX_DELTA_PERCENT->{22} / 100,  #У левого глаза
                            23  => $Const::WIDTH * $MAX_DELTA_PERCENT->{23} / 100,   #Х правого глаза
                            24  => $Const::HEIGHT * $MAX_DELTA_PERCENT->{24} / 100,  #У правого глаза
                          };

my $MAX_RIGHT_LEFT_DIFF = 1;

my $IN_FNAME            = $Const::IN_DIR."/raw.txt";                    #
my $OUT_FNAME           = $Const::OUT_DIR."/out1.txt";                  #


open FILE, "< $IN_FNAME";
open OUT, "> $OUT_FNAME";

my $all_count           = 0;
my $count               = 0;
my $fixations           = 0;
my @lparts;
while (<FILE>){
    my (@parts)         = split $Const::IN_DIVISOR, $_;
    $all_count++;

    #валидация
    my $valid           = 1;
    for my $i (@FIELD_NUM){
        #проверяем что в нужных нам полях не записана всякая хрень
        my $f           = $parts[$i];
        if (!defined $f || !($f =~ m/^[-+]?[0-9]*\.?[0-9]+$/i)          #игнорим те строки где нет нужных полей или в нужных полях не флоат
            || $f == 0                                                  #игнорим те строки где в нужных полях нули
        ){
            $valid      = 0;
            last;
        }
    }

    #

    #если попалась хоть одна хрень игнорим строку
    next if ($valid != 1);

    #для разбора самой первой строки файла
    @lparts             = @parts if (!defined $lparts[0]);


    #считаем сколько времени прошло с пор последнего обработанного кадра и сколько кадров уместилось в этот промежуток времени
    my $dt              = 0;
    $dt                 = $parts[0] - $lparts[0] if (defined $lparts[0]);
    my $frame_count     = $dt / $Const::FRAME_TIME;

    #второй этап валидации
    my $deltas;
    for my $i (@FIELD_NUM){
        #скорости изменения параметров
        $deltas->{$i}   = $parts[$i] - $lparts[$i];

        #проверяем скорость изменения координат
        next if ($MAX_DELTA->{$i} < 1);
        if (abs($deltas->{$i}) > $MAX_DELTA->{$i} * $frame_count){
            $valid      = 0;
            last;
        }
    }

    #проверяем, что скорости изменения по правому и левому глазу совпадают
    if ($valid == 1 &&
        (abs($deltas->{$FIELD_NUM[1]} - $deltas->{$FIELD_NUM[3]}) * $frame_count > $MAX_RIGHT_LEFT_DIFF ||
         abs($deltas->{$FIELD_NUM[2]} - $deltas->{$FIELD_NUM[4]}) * $frame_count > $MAX_RIGHT_LEFT_DIFF)
        ){
        $valid          = 0;
        print "Right-Left delta excess!";
    }

    #если попалась хоть одна хрень игнорим строку
    next if ($valid != 1);

    #фиксация ли в данный момент смотрим. Смотрим по левому глазу, тк ранее уже проверили, что погрешность между правым и левым мала
    my $now_fixation    = $Const::FRAME_TYPE->{Fixation};
    $now_fixation       = $Const::FRAME_TYPE->{Unknown} if ($count < 1 || diag($deltas->{$FIELD_NUM[1]}, $deltas->{$FIELD_NUM[2]}) > $Const::MAX_FIXATION_DELTA);

    #изменения по координатам Х и У. Смотрим только для левого глаза, тк ранее уже проверили, что погрешность между правым и левым мала
    my ($dx, $dy)       = ( int($deltas->{$FIELD_NUM[1]}), int($deltas->{$FIELD_NUM[2]}) );
    my $dv              = int(diag($dx, $dy));

    #пишем в файл
    for my $i (@FIELD_NUM_OUT){
        my $f           = int($parts[$i]);

        print OUT "$f".$Const::OUT_DIVISOR;
        print OUT "$dt".$Const::OUT_DIVISOR if ($i == 0);
    }
    print OUT "$dx".$Const::OUT_DIVISOR;
    print OUT "$dy".$Const::OUT_DIVISOR;
    print OUT "$dv".$Const::OUT_DIVISOR;
    print OUT "$now_fixation".$Const::OUT_DIVISOR;
    print OUT "\n";

    #запоминаем предыдущие значения параметров и двигаем счетчики
    @lparts             = @parts;
    $count++;
    $fixations++ if ($now_fixation == 1);
    #last if ($count > 100);
}

close FILE;
close OUT;

print " Frames: $all_count \n Valid frames: $count \n Fixation frames: $fixations \n";
exit;

1;

sub diag {
    my ($a, $b) = @_;
    return ($a ** 2 + $b ** 2) ** 0.5;
}

=pod
    AUTHOR - Мартынов Сергей
=cut