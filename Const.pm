package Const;
use strict;
use warnings;
use Encode qw/encode decode/;
use Try::Tiny;


our $FRAME_TYPE         = {
                            0 => "Unknown", 1 => "Fixation", 2 => "Saccade",
                            "Unknown" => 0, "Fixation" => 1, "Saccade" => 2,
                          };

our $MIN_SACCADE_TIME   = 20000;                        #
our $MAX_SACCADE_TIME   = 80000;                        #
our $MIN_FIXATION_TIME  = 150000;                       #
our $MAX_FIXATION_TIME  = 2000000;                      #

our $WIDTH              = 1920;
our $HEIGHT             = 1080;
our $DIAG               = diag($WIDTH, $HEIGHT);

our $MAX_FIX_PERCENT    = 1.0;
our $MAX_FIXATION_DELTA = $DIAG * $MAX_FIX_PERCENT / 100;
our $MAX_FIXATION_DV    = $DIAG * $MAX_FIX_PERCENT * 2 / 100;


our $TIME_DIVISOR       = 1000000;                      #на сколько нужно делить печать времени, чтобы получить секунды
our $FRAME_TIME         = 4000;                         #сколько времени (в единицах, представленных в файле) длится 1 кадр

our $WINDOW_SIZE        = 1000;                         #количество кадров, которое умещается в скользящее окно анализа

our $IN_DIVISOR         = "\t";                         #
our $OUT_DIVISOR        = "\t";                         #
our $IN_DIR             = "in";                         #
our $OUT_DIR            = "out";                        #
our $DEBUG_DIR          = "debug";                      #


#загрузить в окно size кадров. Лишние (> WINDOW_SIZE) вытеснить в OUT
sub shift_window {
    my ($F_IN, $F_OUT, $wind, $size, $types_count) = @_;
    #print "shift_window ($size) called\n";

    my @WINDOW          = @{$wind};
    my $now_size        = scalar(@WINDOW);
    # формат:    t       dt      Х       У       dx      dy      dv      тип_кадра

    my $res             = {window => undef, status => -1};
    return $res if ($size < 1 || $size > $WINDOW_SIZE);

    #удаляем из окна лишее. То есть пишем на выход обработанные данные
    my $put_back_size   = max(0, $now_size + $size - $WINDOW_SIZE);
    for (my $i = 0; $i < $put_back_size; $i++){
        my $elem        = shift @WINDOW;
        $types_count->{$elem->{ty}} = ($types_count->{$elem->{ty}} || 0) + 1;
        print $F_OUT (join $OUT_DIVISOR, (
                                        $elem->{ti},
                                        $elem->{dt},
                                        $elem->{x},
                                        $elem->{y},
                                        $elem->{dx},
                                        $elem->{dy},
                                        $elem->{dv},
                                        $elem->{ty}
                                      )).$OUT_DIVISOR."\n";
    }

    #подгружаем новую порцию данных
    my $count           = 0;
    while (<$F_IN>){
        my (@parts)     = split $IN_DIVISOR, $_;
        push @WINDOW, {
                        ti      => int($parts[0]),
                        dt      => int($parts[1]),
                        x       => int($parts[2]),
                        y       => int($parts[3]),
                        dx      => int($parts[4]),
                        dy      => int($parts[5]),
                        dv      => int($parts[6]),
                        ty      => int($parts[7]),
                      };
        $count++;
        last if ($count >= $size);
    }

    if ($count == 0){
        $res->{status}  = -2;
    } else {
        $res->{status}  = 0;
    }
    $res->{window}      = \ @WINDOW;
    $res->{finish}      = ($count < $size) ? 1 : 0;
    $res->{loaded}      = $count;
    $res->{types_count} = $types_count;
    return $res;
}

#посчтитать среднее для всех усредняемых полей кадров на определенном диапазоне кадров в выгруженном окне.
my @avarage_fields      = ("dt", "x", "y", "dx", "dy");
sub average {
    my ($wind, $from, $size, $need_type) = @_;
    $need_type          = 0 if (!defined $need_type || $need_type < 0 || $need_type > 2);

    my @WINDOW          = @{$wind};
    my $now_size        = scalar(@WINDOW);

    return undef if ($from < 0 || $size < 0 || $now_size <= $from + $size);
    my $to              = $from + $size;

    my $avg             = {};
    my $count           = 0;
    for (my $i = $from; $i <= $to; $i++){
        #не считаем те кадры, где тип не совпадает с требуемым
        next if ($need_type != 0 && $WINDOW[$i]->{ty} != $need_type);
        $count++;

        foreach my $field (@avarage_fields){
            $avg->{$field} = ($avg->{$field} || 0) + $WINDOW[$i]->{$field};
        }
    }
    foreach my $field (@avarage_fields){
        $avg->{adx}     = $avg->{dx} if ($field eq 'dx');
        $avg->{ady}     = $avg->{dy} if ($field eq 'dy');
        $avg->{$field}  = int ($avg->{$field} / ($size + 1));
    }
    $avg->{adt}         = abs( $WINDOW[$from]->{ti} - $WINDOW[$to]->{ti} );
    $avg->{dv}          = int( ($avg->{dx} ** 2  + $avg->{dy} ** 2 ) ** 0.5 );
    $avg->{adv}         = int( ($avg->{adx} ** 2 + $avg->{ady} ** 2 ) ** 0.5 );
    return $avg;
}

#помечаем указанный диапазон кадров определенным статусом
sub mark_frames {
    my ($wind, $from, $size, $type) = @_;

    my @WINDOW          = @{$wind};
    my $now_size        = scalar(@WINDOW);

    my $res             = {window => undef, status => -1};
    return $res if ($from < 0 || $size < 0 || $now_size <= $from + $size);
    return $res if (!defined $type || $type < 0 || $type > 2);

    my $to              = $from + $size;
    for (my $i = $from; $i <= $to; $i++){
        $WINDOW[$i]->{ty} = $type;
    }
    $res->{window}      = \ @WINDOW;
    $res->{status}      = 0;
    return $res;
}

#
sub min {
    my ($a, $b) = @_;
    return $a if ($a < $b);
    return $b;
}

#
sub max {
    my ($a, $b) = @_;
    return $a if ($a > $b);
    return $b;
}

#
sub diag {
    my ($a, $b) = @_;
    return ($a ** 2 + $b ** 2) ** 0.5;
}




